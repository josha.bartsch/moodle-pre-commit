#!/usr/bin/env python3

import re
from git import Repo
from argparse import ArgumentParser
from datetime import datetime
from pathlib import Path
from os import environ


def bump(match: str) -> str:
    today = datetime.now().strftime("%Y%m%d")
    version = match.group("ver")
    date = version[:8]
    it = int(version[8:])
    it = 0 if date != today else it + 1
    environ["bumped"] = "1"
    new = f"{today}{it:02}"
    if environ["quiet"] != "1":
        print(f"{version.strip(';')} -> {new}")
    return f"{match.group('pre')}{new};"


def run():
    parser = ArgumentParser()
    parser.add_argument("file", help="Versionfile path", nargs='?', default=Path("version.php"))
    parser.add_argument("-v", "--verbose", help="Verbose: Show old + new version string", required=False, action="store_true")
    args = parser.parse_args()
    environ["quiet"] = "1" if not args.verbose else "0" 

    if not Path(args.file).exists():
        print("version.php not found")
        exit(1)

    repo = Repo()
    index = [diff.a_blob.name for diff in repo.index.diff("HEAD")]
    if "version.php" in index:
        exit(0)

    with open(args.file, "r") as reader:
        lines = reader.readlines()
    content = map(
        lambda line: re.sub(
            r"(?P<pre>\s*\$plugin->version\s*=\s*)(?P<ver>\d{10});", bump, line
        ),
        lines,
    )
    with open(args.file, "w") as writer:
        for line in content:
            writer.write(f"{line}")
    if "bumped" not in environ:
        raise Exception("Invalid version number")
    repo.index.add("version.php")


if __name__ == "__main__":
    run()
